#include "AutoMode.h"

AutoMode::AutoMode(int pin, Scansione* s, PotTask* p){
    Mode::attachButton(pin);
    this->s = s;
    this->pot = p;
    addTask((Task*)s);
    addTask((Task*)pot);
}

void AutoMode::switchOn(){
    this->s->setActive(true);
    this->pot->setActive(true);
    this->s->setInfinite(true);
}

void AutoMode::switchOff(){
    this->s->setActive(false);
    this->s->init(125);
    this->pot->setActive(false);
}
