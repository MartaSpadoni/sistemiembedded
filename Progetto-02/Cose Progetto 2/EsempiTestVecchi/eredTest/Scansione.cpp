#include "Scansione.h"
#include "Arduino.h"

Scansione::Scansione(int echoPin, int trigPin, int servoPin, int ledPin){
  this->radar = new RadarImpl(echoPin, trigPin, servoPin);
  this->led = new Led(ledPin);
}
  
void Scansione::init(int period){
  Task::init(period);
  this->direction = 0;
  this->delta = 5;
  this->radar->on();
  this->isOver = false;
  this->radar->setPosition(this->direction-this->delta);
  
}
  
void Scansione::tick(){
  if(isActive()){
    this->radar->scan();
    this->isOver = this->direction + this->delta > 180 || this->direction + this->delta < 0;
    this->direction += this->delta;
    if(isOver){
      this->direction = 0;
      this->radar->setPosition(this->direction);
      setActive(false);
    }else{
    this->radar->setPosition(this->direction);
    }
  }
  
}
