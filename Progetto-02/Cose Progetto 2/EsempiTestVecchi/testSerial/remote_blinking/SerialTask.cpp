#include "SerialTask.h"
#include "Arduino.h"

  
SerialTask::SerialTask(ModeTask* m){ 
  this->m = m;
  this->virtualIsPressed = false;
}

void SerialTask::init(int period){
  Task::init(period);
  MsgService.init();
  
}
  
void SerialTask::tick(){
    if (MsgService.isMsgAvailable()){
    Msg* msg = MsgService.receiveMsg();    
    if (msg->getContent() == "1"){
       this->virtualIsPressed = true;
       MsgService.sendMsg("CAMBIO DI MODALITA");
    }else{
      this->virtualIsPressed = false;
    }
    /* NOT TO FORGET: msg deallocation */
    delete msg;
  }
}

bool SerialTask::virtualButtonIsPressed(){
  return this-> virtualIsPressed;
}
