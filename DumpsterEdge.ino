#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>

/* wifi network name */
char* ssidName = "Matteo";
/* WPA2 PSK password */
char* pwd = "ciaociao";
/* service IP address */ 
char* address = "http://77ecf2f8.ngrok.io";

float lastWeight = 0;

bool avl = true;

void setup() { 
  Serial.begin(115200);                                
  WiFi.begin(ssidName, pwd);
  Serial.print("Connecting...");
  while (WiFi.status() != WL_CONNECTED) {  
    delay(500);
    Serial.print(".");
  } 
  Serial.println("Connected: \n local IP: "+WiFi.localIP());
}

int sendData(String address, float value){  
   HTTPClient http;    
   http.begin(address + "/api/esp");      
   http.addHeader("Content-Type", "application/json");    
   int retCode = http.POST(String(value));   
   http.end();  
      
   // String payload = http.getString();  
   // Serial.println(payload);      
   return retCode;
}

int notAvailable(String address){
  HTTPClient http;    
   http.begin(address + "/api/esp");      
   http.addHeader("Content-Type", "application/json");    
   int retCode = http.PUT("false");   
   http.end();        
   return retCode;
}
   
void loop() { 
   if (WiFi.status()== WL_CONNECTED){   
    if(avl){
      /*1023 * 48,88 ~= 50000g (ipotizziamo che il bidone supporti 50kg)
     * mandiamo un messaggio al service solo se la variazione e' superiore a 200g
     * in modo da tener conto delle possibili variazioni elettriche del pot
     */
  
     /* read sensor */
     float value = analogRead(A0)*48.88;
     Serial.println(value);
     if(value-lastWeight >= 200){
  
         Serial.print("sending "+String(value-lastWeight)+"...");    
         int code = sendData(address, value-lastWeight);
      
         /* log result */
         if (code == 200){
           Serial.println("ok");   
           lastWeight = value;
         } else {
           Serial.println("error");
         }
         if(lastWeight == wMax) avl=false;
      }else{
          Serial.println("no variazione");
      }
    }else{
      notAvailable(address);
    }
    
   } else { 
     Serial.println("Error in WiFi connection");   
   }
   
   delay(2000);  

}
