
#ifndef __SINGLEMODE__
#define __SINGLEMODE__

#include "Mode.h"
#include "Scansione.h"
#include "PotTask.h"
#include "PirDetection.h"

class SingleMode: public Mode {

    Scansione* s;
    PirDetection* pir;
    PotTask* pot;


public:
    SingleMode(int pin, Scansione* s, PirDetection* pir, PotTask* pot);
    void switchOn();
    void switchOff();
};

#endif
