#include "ModeTask.h"
#include "Arduino.h"

ModeTask::ModeTask(){
    currentMode = 0; 
    nModes = 0;
}
void ModeTask::init(int period){
    Task::init(period);
}

void ModeTask ::addMode(Mode* m){
    this->modes[nModes] = m;
    nModes++;
}

void ModeTask::tick(){
  for(int i = 0; i < nModes; i++){
    if(this->modes[i]->isActive()){
      Serial.println("premuto");
      this->modes[currentMode]->switchOff();
      this->currentMode = i;
      this->modes[i]->switchOn();    
//      this->modes[currentMode]->switchOn();
//     currentMode = i;
//      this->modes[currentMode]->switchOn();
      }
    }
      //Serial.println("current Mode: " + String(currentMode));
}
