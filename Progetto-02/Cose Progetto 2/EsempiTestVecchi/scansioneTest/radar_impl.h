#ifndef __RADAR_IMPL__
#define __RADAR_IMPL__

#include "radar.h"
#include "sonar_impl.h"
#include "servo_motor_impl.h"
#include "Arduino.h"

class RadarImpl: public Radar {

public: 
  RadarImpl(int echoSonarPin, int trigSonarPin, int servoPin);
  void on();
  void off();
  void setPosition(int angle);
  float getDistance();
  bool sonarDetection();
    
private:
    ServoMotor *motor; 
    Sonar *sonar;
};

#endif
