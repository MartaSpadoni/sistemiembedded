package seiot.modulo_lab_2_2.msg;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import javax.swing.*;

/**
 * Testing simple message passing.
 * 
 * To be used with an Arduino connected via Serial Port running
 * modulo-2.3/remote_blinking.ino
 * 
 * @author aricci
 *
 */
public class TestRemoteBlinking {

	public static void main(String[] args) throws Exception {
		SerialCommChannel channel = new SerialCommChannel("/dev/ttyACM0", 9600);
		JFrame frame = new JFrame("My First GUI");
		JPanel canvas = new JPanel();
		canvas.setLayout(new BorderLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(300, 300);
		JTextArea textArea = new JTextArea();
		//textArea.setAutoscrolls(true);
		canvas.add(textArea, BorderLayout.CENTER);
		JButton button1 = new JButton("Cambia modalita");
		canvas.add(button1, BorderLayout.SOUTH);
		frame.setContentPane(canvas);

		button1.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				channel.sendMsg("1");
			}
		});
		frame.setVisible(true);
		   
		System.out.println("Waiting Arduino for rebooting...");		
		Thread.sleep(4000);
		System.out.println("Ready.");	

		
		while (true){
			String msg = channel.receiveMsg();
			textArea.setText(msg);
			//textArea.append(msg+"\n");
		}
	}

}
