#include "Scheduler.h"
#include "Scansione.h"
#include "BlinkTask.h"
#include "PirDetection.h"
#include "PotTask.h"

Scheduler sched;

void setup(){

  Serial.begin(9600);
  sched.init(125);

  Task* t1 = new Scansione(7,8,2,13);
  t1->init(125);
  t1->setActive(false);
  sched.addTask(t1);

  
  Task* t2 = new PotTask(A0,t1);
  t2->init(1000);
  t2->setActive(true);
  sched.addTask(t2);


  Task* t3 = new PirDetection(11, t1);
  t3->init(125);
  t3->setActive(true);
  sched.addTask(t3);
   
}

void loop(){
  sched.schedule();
}
