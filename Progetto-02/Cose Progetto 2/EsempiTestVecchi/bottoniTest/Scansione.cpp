#include "Scansione.h"
#include "Arduino.h"

Scansione::Scansione(int echoPin, int trigPin, int servoPin, int ledPin){
  this->radar = new RadarImpl(echoPin, trigPin, servoPin);
  this->led = new Led(ledPin);
  this->infinity = false;
}
  
void Scansione::init(int period){
  Task::init(period);
  this->direction = 0;
  this->delta = 12;
  this->radar->on();
  this->isOver = false;
  this->radar->setPosition(this->direction-this->delta);
  
}
  
void Scansione::tick(){
    this->radar->scan();
    if(this->radar->detected()){
      this->led->switchOn();
      delay(50);
      this->led->switchOff();
    }
    this->direction += this->delta;
    this->isOver = this->direction > 180 || this->direction < 0;
    if(isOver){
      this->direction = 0;
      if(!infinity){
      setActive(false);
      }
    }
    this->radar->setPosition(this->direction);
}

void Scansione::setInfinite(bool inf){
  this->infinity = inf;
  
}

bool Scansione::hasFinished(){
  return isOver;
}
