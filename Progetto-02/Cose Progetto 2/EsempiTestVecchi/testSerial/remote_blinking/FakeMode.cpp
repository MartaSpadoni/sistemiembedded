#include "FakeMode.h"
#include "Arduino.h"

FakeMode::FakeMode(int pin, BlinkTask* b){
    Mode::attachButton(pin);
    this->b = b;
    addTask((Task*)b);
}

void FakeMode::switchOn(){
    this->b->setActive(true);
}

void FakeMode::switchOff(){
    this->b->setActive(false);
}
