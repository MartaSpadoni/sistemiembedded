#include "pir_impl.h"
#define LED_PIN 13
Pir* p;
bool prima= false;
long t0 = 0;
void setup(){
  p = new PirImpl(11);
  pinMode(LED_PIN,OUTPUT);
  Serial.begin(9600);
  p->calibrate();
}

void loop(){
  if (p->hasDetected()){
    if(prima == false){
      t0 = millis();
      prima = true;
    }
    Serial.println("high");
    digitalWrite(LED_PIN, HIGH);
  }else{
    if(prima){
      t0 = millis()-t0;
      prima = false;
      Serial.println(t0);
    }
  }
};
