#ifndef __POTTASK__
#define __POTTASK__

#include "Task.h"
#include "pot_impl.h"
#include "Scansione.h"

class PotTask: public Task {

Pot* pot;
Scansione* s;

public:

  PotTask(int pin, Scansione*s);  
  void init(int period);  
  void tick();
};

#endif
