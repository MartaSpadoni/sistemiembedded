#ifndef __SERIALTASK__
#define __SERIALTASK__

#include "Task.h"
#include "MsgService.h"
#include "ModeTask.h"

class SerialTask: public Task {

ModeTask* m;
char data;
bool virtualIsPressed;

public:
  SerialTask(ModeTask* m);
  void init(int period);  
  void tick();
  bool virtualButtonIsPressed();
};


 

#endif
