#include "PotTask.h"
#include "Arduino.h"

PotTask::PotTask(int pin, Scansione* s){
  this->pot = new PotImpl(pin); 
  this->s = s;
}
  
void PotTask::init(int period){
  Task::init(period);
}
  
void PotTask::tick(){
  if(this->s->hasFinished()){
    int v = this->pot->getValue();
    int m = map(v, 0, 1023, 1, 5);
    Serial.println("Mappato in: " + String(m));
    this->s->init(m*125);
  }
}
