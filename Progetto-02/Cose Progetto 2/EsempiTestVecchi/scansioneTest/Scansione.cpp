#include "Scansione.h"
#include "Arduino.h"

Scansione::Scansione(int echoPin, int trigPin, int servoPin, int ledPin){
  this->radar = new RadarImpl(echoPin, trigPin, servoPin);
  this->led = new Led(ledPin);
}
  
void Scansione::init(int period){
  Task::init(period);
  this->direction = 0;
  this->delta = 5;
  this->radar->on();
  this->radar->setPosition(0);
  
}
  
void Scansione::tick(){
  Serial.println(this->radar->getDistance());
  this->delta = this->direction + this->delta > 180 || this->direction + this->delta < 0 ? - this->delta : this->delta;
  this->direction += this->delta;
  if(this->direction == 180 || this->direction == 0){
    this->radar->off();
  }else{
  this->radar->setPosition(this->direction);
  }
  
}
