#ifndef __RADAR_IMPL__
#define __RADAR_IMPL__

#include "sonar_impl.h"
#include "servo_motor_impl.h"
#include "Arduino.h"

class RadarImpl: public ServoMotorImpl, public SonarImpl {
  bool objectDetected;

public: 
  RadarImpl(int echoSonarPin, int trigSonarPin, int servoPin);
  void scan();
  bool detected();
};

#endif
