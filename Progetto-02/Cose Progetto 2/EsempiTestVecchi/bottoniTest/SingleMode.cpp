#include "SingleMode.h"

SingleMode::SingleMode(int pin, Scansione* s, PirDetection* pir, PotTask* pot){
    Mode::attachButton(pin);
    this->s = s;
    this->pir = pir;
    this->pot = pot;
    addTask((Task*)s);
    addTask((Task*)pir);
    addTask((Task*)pot);
}

void SingleMode::switchOn(){
    this->s->setActive(false);
    this->pir->setActive(true);
    this->pot->setActive(true);
    this->s->setInfinite(false);
}

void SingleMode::switchOff(){
    this->s->setActive(false);
    this->s->init(125);
    this->pir->setActive(false);
    this->pot->setActive(false);
}
