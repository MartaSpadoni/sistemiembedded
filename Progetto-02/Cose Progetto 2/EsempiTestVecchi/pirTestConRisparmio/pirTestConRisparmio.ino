#include "pir_impl.h"
#include <avr/sleep.h>
#define LED_PIN 13
Pir* p;
bool prima= false;
long t0 = 0;

void wakeUpNow(){}

void setup(){
  p = new PirImpl(11);
  pinMode(LED_PIN,OUTPUT);
  attachInterrupt(digitalPinToInterrupt(2),wakeUpNow, RISING); // use interrupt 0 (pin 2) and run function
  //PER FARLO SCATTARE ANCHE ALLA PRESSIONE DEL PULSANTE
  attachInterrupt(digitalPinToInterrupt(3),wakeUpNow, HIGH); // use interrupt 0 (pin 2) and run function
  Serial.begin(9600);
  p->calibrate();
}

void loop(){
  Serial.println("GOING IN POWER DOWN");
  t0=millis();
  Serial.println("tempo prima di entrare " + String(t0));
  Serial.flush();
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);  
  sleep_enable();
  sleep_mode();  
  t0 = millis()-t0;
  Serial.println("tempo all'uscita " + String(t0));
  Serial.println("exit from sleep for a detection");
  sleep_disable();
  digitalWrite(LED_PIN, HIGH);
  delay(500);
  digitalWrite(LED_PIN, LOW);
};
