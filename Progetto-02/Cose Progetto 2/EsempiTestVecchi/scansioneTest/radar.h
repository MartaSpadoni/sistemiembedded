#ifndef __RADAR__
#define __RADAR__

class Radar {

public:
  virtual void on() = 0;
  virtual void off() = 0;
  virtual void setPosition(int angle) = 0;
  virtual float getDistance() = 0;
  virtual bool sonarDetection()= 0;

};

#endif
