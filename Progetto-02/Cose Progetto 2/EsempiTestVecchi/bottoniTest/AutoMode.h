#ifndef __AUTOMODE__
#define __AUTOMODE__

#include "Mode.h"
#include "Scansione.h"
#include "PotTask.h"

class AutoMode: public Mode {

    Scansione* s;
    PotTask *pot;


public:
    AutoMode(int pin, Scansione* s , PotTask* p);
    void switchOn();
    void switchOff();
};

#endif
