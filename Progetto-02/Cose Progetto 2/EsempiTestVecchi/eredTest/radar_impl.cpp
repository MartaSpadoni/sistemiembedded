#include "radar_impl.h"

RadarImpl::RadarImpl(int echoSonarPin, int trigSonarPin, int servoPin):ServoMotorImpl(servoPin),SonarImpl(echoSonarPin,trigSonarPin){
} 

void RadarImpl::scan(){
    if(detection()){
      Serial.println("Rilevato oggetto in posizione " + String(getPosition()) + " a distanza: "+String(getDistance()));
    }else{
      Serial.println("Nessun rilevamento" );
    }
}
