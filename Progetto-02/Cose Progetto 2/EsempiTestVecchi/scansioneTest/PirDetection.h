#ifndef __PIRDETECTION__
#define __PIRDETECTION__

#include "Task.h"
#include "pir_impl.h"

class PirDetection: public Task {

Pir* pir;

public:

  PirDetection(int pin);  
  void init(int period);  
  void tick();
};

#endif