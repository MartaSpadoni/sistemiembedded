#include "Scheduler.h"
#include "Scansione.h"
#include "BlinkTask.h"
#include "PirDetection.h"

Scheduler sched;

void setup(){

  Serial.begin(9600);
  sched.init(100);
  
  Task* t1 = new Scansione(7,8,2,13);
  t1->init(500);
  sched.addTask(t1);

  Task* t2 = new BlinkTask(12);
  t2->init(100);
  //sched.addTask(t2);

  Task* t3 = new PirDetection(11);
  t3->init(1000);
  sched.addTask(t3);
   
}

void loop(){
  sched.schedule();
}
