#include "radar_impl.h"

RadarImpl::RadarImpl(int echoSonarPin, int trigSonarPin, int servoPin):ServoMotorImpl(servoPin),SonarImpl(echoSonarPin,trigSonarPin){
  objectDetected = false;
} 

void RadarImpl::scan(){
    float d = getDistance();
    if(detection(d)){
      objectDetected = true;
      Serial.println("Rilevato oggetto in posizione " + String(getPosition()) + " a distanza: "+String(d));
    }else{
      Serial.println("Nessun rilevamento" );
      objectDetected = false;
    }
}

bool RadarImpl::detected(){
  return this->objectDetected;
}
