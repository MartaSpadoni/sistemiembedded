#ifndef __POTTASK__
#define __POTTASK__

#include "Task.h"
#include "pot_impl.h"
#include "Scansione.h"

class PotTask: public Task {

Pot* pot;
Task* s;

public:

  PotTask(int pin, Task*s);  
  void init(int period);  
  void tick();
};

#endif
