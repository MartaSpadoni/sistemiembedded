#ifndef __FAKEEMODE__
#define __FAKEMODE__

#include "Mode.h"
#include "BlinkTask.h"

class FakeMode: public Mode {

    BlinkTask* b;


public:
    FakeMode(int pin, BlinkTask* b );
    void switchOn();
    void switchOff();
};

#endif
