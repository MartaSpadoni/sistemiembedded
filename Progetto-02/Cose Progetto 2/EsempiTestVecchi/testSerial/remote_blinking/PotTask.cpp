#include "PotTask.h"
#include "Arduino.h"

PotTask::PotTask(int pin, Task* s){
  this->pot = new PotImpl(pin); 
  this->s = s;
}
  
void PotTask::init(int period){
  Task::init(period);
}
  
void PotTask::tick(){
  if(!this->s->isActive()){
    int v = this->pot->getValue();
    int m = map(v, 0, 1023, 1, 5);
    Serial.print("Pot: " + String(v));
    Serial.println("Mappato in: " + String(m));
    this->s->init(m*125);
  }
}
