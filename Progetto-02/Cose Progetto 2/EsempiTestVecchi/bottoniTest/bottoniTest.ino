#include "Scheduler.h"
#include "Scansione.h"
#include "BlinkTask.h"
#include "PirDetection.h"
#include "PotTask.h"
#include "ModeTask.h"
#include "Mode.h"
#include "SingleMode.h"
#include "AutoMode.h"
#include "FakeMode.h"

Scheduler sched;

void setup(){

  Serial.begin(9600);
  sched.init(25);


  Task* t1 = new Scansione(7,8,2,13);
  t1->init(125);
  t1->setActive(false);
  sched.addTask(t1);
  
  Task* t2 = new PotTask(A0,(Scansione*)t1);
  t2->init(125);
  t2->setActive(false);
  sched.addTask(t2);


  Task* t3 = new PirDetection(11, t1);
  t3->init(125);
  t3->setActive(false);
  sched.addTask(t3);

  Task* t4 = new BlinkTask(12);
  t4->init(500);
  t4->setActive(true);
  sched.addTask(t4);
  
  Mode* single = new SingleMode(3, (Scansione*)t1, (PirDetection*)t3, (PotTask*)t2);
  Mode* autoMode = new AutoMode(4, (Scansione*)t1, (PotTask*)t2);
  Mode* fm = new FakeMode(5,(BlinkTask*)t4);
  ModeTask* mt = new ModeTask();
  mt->addMode(fm);
  mt->addMode(single);
  mt->addMode(autoMode);
  mt->init(100);
  mt->setActive(true);
  sched.addTask((Task*)mt);
    
}

void loop(){
  sched.schedule();
}
