#ifndef __MODE__
#define __MODE__

#include "Task.h"
#include "ButtonImpl.h"
#define MAX_TKS 10

class Mode {
    
    int nTasks;
    Task* taskList[MAX_TKS];
    Button* button;
 
public: 

void attachButton(int pin){
    this->button = new ButtonImpl(pin);
}

bool isActive(){
    return this->button->isPressed();
}

virtual void switchOn() = 0;
virtual void switchOff() = 0;

void addTask(Task* t){
    if (nTasks < MAX_TKS-1){
    this->taskList[nTasks] = t;
    nTasks++;
    }
}

};

#endif
