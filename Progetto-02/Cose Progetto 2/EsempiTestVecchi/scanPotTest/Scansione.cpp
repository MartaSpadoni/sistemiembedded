#include "Scansione.h"
#include "Arduino.h"

bool primo = false;
int t0=0;

Scansione::Scansione(int echoPin, int trigPin, int servoPin, int ledPin){
  this->radar = new RadarImpl(echoPin, trigPin, servoPin);
  this->led = new Led(ledPin);
}
  
void Scansione::init(int period){
  Task::init(period);
  this->direction = 0;
  this->delta = 12;
  this->radar->on();
  this->isOver = false;
  this->radar->setPosition(this->direction-this->delta);
  
}
  
void Scansione::tick(){
    if(primo == false){
      primo = true;
      t0 = millis();
    }
    this->radar->scan();
    if(this->radar->detected()){
      this->led->switchOn();
      delay(50);
      this->led->switchOff();
    }
    this->direction += this->delta;
    this->isOver = this->direction > 180 || this->direction < 0;
    if(isOver){
      Serial.println(millis()-t0);
      primo = false;
      t0=0;
      this->direction = 0;
      setActive(false);
    }
    this->radar->setPosition(this->direction);
}
