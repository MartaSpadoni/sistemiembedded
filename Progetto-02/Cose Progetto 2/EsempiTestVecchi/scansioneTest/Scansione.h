#ifndef __SCANSIONE__
#define __SCANSIONE__

#include "Task.h"
#include "radar_impl.h"
#include "Led.h"

class Scansione: public Task {

  int direction;
  Radar* radar;
  Led* led;
  int delta;

public:

  Scansione(int echoPin, int trigPin, int servoPin, int ledPin);  
  void init(int period);  
  void tick();
};


 

#endif
