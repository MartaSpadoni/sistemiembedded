#include "radar_impl.h"

RadarImpl::RadarImpl(int echoSonarPin, int trigSonarPin, int servoPin){
  motor = new ServoMotorImpl(servoPin);
  sonar = new SonarImpl(echoSonarPin,trigSonarPin);
} 

void RadarImpl::on(){
    motor->on();
}
void RadarImpl::off(){
    motor->off();
}
void RadarImpl::setPosition(int angle){
    motor->setPosition(angle);
}
float RadarImpl::getDistance(){
    sonar->getDistance();
}
