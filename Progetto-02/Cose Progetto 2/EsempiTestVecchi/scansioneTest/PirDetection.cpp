#include "PirDetection.h"
#include "Arduino.h"

PirDetection::PirDetection(int pin){
  this->pir = new PirImpl(pin); 
}
  
void PirDetection::init(int period){
  Task::init(period);
  this->pir->calibrate();
}
  
void PirDetection::tick(){
    Serial.println(this->pir->hasDetected());
}
