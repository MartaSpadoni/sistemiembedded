#ifndef __SONAR__
#define __SONAR__

class Sonar{

    public:
        virtual float getDistance() = 0;
        virtual bool detection(float dist) = 0;
};

#endif
