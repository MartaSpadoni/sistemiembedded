#include "PirImpl.h"
#include "Arduino.h"
#include "MsgService.h"

PirImpl::PirImpl(int pin){
    this->pin = pin;
    pinMode(pin, INPUT);
}

void PirImpl::calibrate(){
//    //give the sensor some time to calibrate
//    Serial.print("Calibrating sensor...");
    for(int i = 0; i < calibrationTimeSec; i++){
        delay(1000);
    }
   MsgService.sendMsg("C");
    //Serial.println(" done");
    //Serial.println("PIR SENSOR READY.");
}

int PirImpl::hasDetected(){
    return digitalRead(pin) == HIGH;
}
