#ifndef __PIRDETECTION__
#define __PIRDETECTION__

#include "Task.h"
#include "pir_impl.h"
#include "Scansione.h"

class PirDetection: public Task {

Pir* pir;
Task* s;

public:

  PirDetection(int pin, Task*s);  
  void init(int period);  
  void tick();
};

#endif
