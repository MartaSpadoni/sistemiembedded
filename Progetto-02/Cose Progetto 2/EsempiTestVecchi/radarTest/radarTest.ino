#include "radar_impl.h"

int pos;   
int delta;
Radar *r;

void setup() {
  Serial.begin(9600);
  r = new RadarImpl(7,8,2);
  pos = 0;
  delta = 5;
}

void loop() {
  r->on();
  for (int i = 0; i < 180; i+=5) {
    Serial.println(pos);
    r->setPosition(pos);
    delay(20);                   
    pos += delta;
  }
  r->off();
  pos -= delta;
  delta = -delta;
  delay(1000);
}
