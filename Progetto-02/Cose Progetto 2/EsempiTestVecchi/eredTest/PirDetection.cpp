#include "PirDetection.h"
#include "Arduino.h"

PirDetection::PirDetection(int pin, Task* s){
  this->pir = new PirImpl(pin); 
  this->s = s;
}
  
void PirDetection::init(int period){
  Task::init(period);
  this->pir->calibrate();
}
  
void PirDetection::tick(){
  if(!s->isActive()){
    bool d = this->pir->hasDetected();
    Serial.println(d);
    this->s->setActive(d);
  }
}
